## About The Project

Project description.

### Motivation (optional)

Why project was created, what problem it solves.

### SolDef link

## Getting Started

### Prerequisites

What is needed to install and run project (platform, software, libraries etc....)

- OS (dev/target)
- compiler/runtime required (Java/Node.js/JS?)
- dependencies (build time and run time)

### Installation

Step-by-step guide to project installation.

## Usage

### How to run project

- target environment (backend/browser etc with versions tested specified)

### How to run tests.
### How to lint project.

## Deployment

How to deploy project to all required envs.

## Monitoring

- link to splunk or other place to see the logs
- link to dashboards (New Relic / Splunk)

## License (optional)

## Contact

