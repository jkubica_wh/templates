# Contributing to (your project name)

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 

Seriously, contact owners first =)

## What should I know before I get started?

Read README.md

Try to install and setup project.

## Pull Request Process

### Issue and Pull Request Labels

### Git Commit Messages

### Styleguide
